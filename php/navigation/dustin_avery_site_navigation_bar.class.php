<?php
/**
 * Created by PhpStorm.
 * User: odin
 * Date: 12/16/18
 * Time: 3:53 PM
 */

class Dustin_Avery_Site_Navigation_Bar extends Fixed_Navigation_Bar{

    protected $navigationTitle = "";

    protected $itemList = ["About Me","Projects (Pending)","Goals"];

    protected $customNavClass = "custom-nav-bar";

    protected $customNavContentClass = "custom-nav-content";

    protected $customNavItemClass = "custom-nav-item";

}