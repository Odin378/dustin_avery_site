<?php
/**
 * Created by PhpStorm.
 * User: odin
 * Date: 12/9/18
 * Time: 6:24 AM
 */

class Dustin_Avery_Site_Shell extends Shell{

    public static $root = "/var/www/html/dustin_avery_site/";

    protected function getWebsiteBody()
    {
        $body = new HTML_Element("body");
        $body->text = new Dustin_Avery_Site_Navigation_Bar();

        return $body;


    }
}