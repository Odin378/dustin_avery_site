<?php
//Dustin Avery, main file

//Errors and required adding(Don't know why autoloader wasn't doing it but whatever)
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once "../web_resources/php/autoloader/autoload.class.php";
//Start session and autoloader
session_start();
$autoloader = new Autoload(["php", "../web_resources/php"]);

//Prepared to parse ajax request if they show up
if(isset($_POST['ajax']) && $_POST['ajax']){
    $filename = filter_input(INPUT_POST,"filename",FILTER_SANITIZE_STRING);
    include $filename;
}
//Run the normal page
else {
    $shell = new Dustin_Avery_Site_Shell();
    echo $shell->getWebPage();
}